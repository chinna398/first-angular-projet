import { Injectable } from '@angular/core';
import {Http,Response,Headers,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http:Http) { }
  saveUserData(user)
  {
    // debugger;
    console.log(user);
    return this.http.post('http://localhost:3000/saveUserData',user).map((res:Response)=>res.json());
  }
  userLogin(user){
    return this.http.post('http://localhost:3000/userLogin',user).map((res:Response)=>res.json());
  }
}
