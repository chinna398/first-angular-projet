import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username = "";
  password = "";
  user:any={
    'username':'',
    'password':''
  };
  constructor(private backendService:BackendService) { }

  ngOnInit() {
  }
  login(){
    if(this.username=="" || this.password =="")
    {
      alert("Please enter username and password");
    }
    else{
      alert("Username: " + this.username + "\n" + "Password: " + this.password);
      this.user ={
        'username':this.username,
        'password':this.password
      }
      this.backendService.userLogin(this.user).subscribe(data =>{alert(data.message)});
    }
  }
}
