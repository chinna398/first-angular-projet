import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { jsonpFactory } from '@angular/http/src/http_module';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  formVlues :any={
    'name':'',
    'email':'',
    'password':'',
  };
  imageFile : File=null;
  constructor(private backendService:BackendService) { }

  ngOnInit() {
  }
  onUploadImage(event)
  {
    console.log(event);
    this.imageFile = event.target.files[0];
  }
  register(form) {
    this.formVlues = form.value;
    
    console.log(this.formVlues);
    this.backendService.saveUserData(this.formVlues).subscribe(data =>{alert(data.message)});
    alert("Name:" + this.formVlues.name + "\n" + "E-Mail:" + this.formVlues.email + "\n" + "Password:" + this.formVlues.password);
}
}
